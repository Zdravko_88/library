import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MaterialModule } from './material.module';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { AddBookComponent } from '@Books/components/add-book/add-book.component';
import { BooksListComponent } from '@Books/components/books-list/books-list.component';
import { BooksHomeComponent } from '@Books/components/books-home/books-home.component';

import { BooksService } from '@Books/books.service';

@NgModule({
  declarations: [
    AppComponent,
    BooksListComponent,
    AddBookComponent,
    BooksHomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    MatTableModule,
    MatInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [
    BooksService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
