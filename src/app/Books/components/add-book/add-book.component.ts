import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BooksService } from "../../books.service";

import { FormGroup,FormControl,Validators } from '@angular/forms';

@Component({
  selector: 'zdravko-add-book',
  templateUrl: './add-book.component.html',
})

export class AddBookComponent implements OnInit {
  public addNewBookForm: FormGroup;

  newBookList = JSON.parse(localStorage.getItem('Books'));
  
  constructor(private booksService: BooksService, private router: Router) { }

  ngOnInit() {
  }

  public book = {};

  addBookForm = new FormGroup({
    id: new FormControl(),
    serialNumber: new FormControl(),
    name: new FormControl(),
    authors: new FormControl(),
    bookQty: new FormControl(),
    bookQtyCopy: new FormControl()
  });

  public createNewBook = () => {
    this.book = this.addBookForm.value;
    this.newBookList.push(this.book);
    localStorage.setItem('Books',JSON.stringify(this.newBookList));
    console.log("New Book ====>", this.book);
    console.log("Books List ====>", this.newBookList);
    this.router.navigate(['/book-list']);
  }

}





