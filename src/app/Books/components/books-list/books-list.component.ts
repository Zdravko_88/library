import { Component, OnInit, ViewChild } from '@angular/core';
import { BooksService } from "../../books.service";
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'zdravko-books-list',
  templateUrl: './books-list.component.html',
})

export class BooksListComponent implements OnInit {
  title: string = 'Book List';

  public displayedColumns: string [] = ['serialNumber','name','authors','bookQty','bookQtyCopy','actions'];
  dataSource = new MatTableDataSource(JSON.parse(localStorage.getItem('Books')));
  
 @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private booksServices: BooksService){ }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  removeBook(elm: number) {
    this.dataSource.data = this.dataSource.data.filter(i => i !== elm);
    localStorage.setItem('Books', JSON.stringify(this.dataSource.data));
  }
}

