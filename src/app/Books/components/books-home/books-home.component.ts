import { Component, OnInit } from '@angular/core';
import { BooksService } from "./../../books.service";

@Component({
  selector: 'zdravko-books-home',
  templateUrl: './books-home.component.html',
})

export class BooksHomeComponent implements OnInit {

  constructor(private booksServices: BooksService) { }

  ngOnInit() {
    this.booksServices.saveToLocal();
  }

}
