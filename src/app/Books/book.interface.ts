export interface Book{
    id: number;
    serialNumber: string | number;
    name: string;
    authors: string;
    bookQty: number;
    bookQtyCopy: number;
}