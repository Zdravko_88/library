import { Injectable } from '@angular/core';
import { Book } from './book.interface';

export class BooksService {

  books: Book[] = [
    {
      "id": 1,
      "serialNumber": 123,
      "name": "Three Hearts and Three Lions",
      "authors": "Poul Anderson",
      "bookQty": 20,
      "bookQtyCopy": 20,
    },
    {
      "id": 2,
      "serialNumber": 223,
      "name": "Alice's Adventures in Wonderland",
      "authors": "Lewis Carroll",
      "bookQty": 20,
      "bookQtyCopy": 20
    },
    {
      "id": 3,
      "serialNumber": 222,
      "name": "The Lion, the Witch and the Wardrobe",
      "authors": "C.S. Lewis",
      "bookQty": 20,
      "bookQtyCopy": 20
    },
    {
      "id": 4,
      "serialNumber": 234,
      "name": "The Lord of the Rings Trilogy",
      "authors": "J.R.R. Tolkien",
      "bookQty": 20,
      "bookQtyCopy": 20
    }
  ];

  constructor() { }

  getBooks() {
    return this.books;
  }

  parsed_json;

  saveToLocal(){
    if(localStorage.getItem('Books')){

    } else {
      this.parsed_json = JSON.stringify(this.books);
      localStorage.setItem('Books', this.parsed_json);
    }
  }
  
}
