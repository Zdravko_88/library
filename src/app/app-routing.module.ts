import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BooksListComponent } from '@Books/components/books-list/books-list.component';
import { AddBookComponent } from '@Books/components/add-book/add-book.component';
import { BooksHomeComponent } from '@Books/components/books-home/books-home.component';

const routes: Routes = [
  { path: 'book-list', component: BooksListComponent },
  { path: 'add-new-book', component: AddBookComponent },
  { path: 'home', component: BooksHomeComponent },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
